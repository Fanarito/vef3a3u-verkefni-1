$(document).ready(function ($) {
    var server = "http://media.fanarito.duckdns.org";

    var accessToken;

    if(Cookie.get('embyAccess')){
        accessToken = Cookie.get('embyAccess');
    }

    var updateUI = function () {
        // Toggle sidebar
        $(".launch").click(function () {
            $('.ui.sidebar').sidebar('toggle');
        });
    }

    // Models
    var Users = Backbone.Collection.extend({
        url: server + "/users/public"
    });

    // Views
    var LoginView = Backbone.View.extend({
        el: "#full",
        
        events: {
            // On card click show password box
            "click .ui.card": "showPasswordBox",
            "click .ui.icon.button.login" : "login"
        },

        render: function () {
            var that = this;
            var users = new Users();
            users.fetch({
                success: function (users) {
                    $.get('templates/login.html', function (template) {
                        var rendered = Mustache.render(template, { users: users.models });
                        that.$el.html(rendered);
                        updateUI();
                    }, 'html');
                }
            });
        },

        // Function called when .ui.card is clicked on
        showPasswordBox: function (e) {
            $(e.currentTarget).find('.ui.input').slideDown();
            $(e.currentTarget).find('.ui.input').find('input').focus();
        },

        // Function called when you click on the login button
        login: function (e) {
            var username = $(e.target).data('username');
            // Create sha1 and md5 hashes of password
            var password = CryptoJS.SHA1($(e.target).prev('input').val()).toString(CryptoJS.enc.Hex);
            var passwordMD5 = CryptoJS.MD5($(e.target).prev('input').val()).toString(CryptoJS.enc.Hex);

            alert(username);
            alert(password);
            alert(passwordMD5);

            var requestUrl = server + "/Users/AuthenticateByName";
            var dataToSend = {
                username: username,
                password: password,
                passwordMd5 :  passwordMD5
            }

            $.post(requestUrl, dataToSend, function (data, textStatus) {
                accessToken = data.AccessToken;
                Cookie.set('embyAccess', accessToken);
                loginView.unbind();
                //loginView.remove();
                homeView.render();
                router.navigate("home", { trigger: true, replace: true });
            }, "json");
        }
    });

    var HomeView = Backbone.View.extend({
        el: "#full",

        events: {

        },

        render: function () {
            var that = this;
            var users = new Users();
            users.fetch({
                success: function (users) {
                    $.get('templates/home.html', function (template) {
                        var rendered = Mustache.render(template, { users: users.models });
                        that.$el.html(rendered);
                        updateUI();
                    }, 'html');
                }
            });
        }
    });

    // Routes
    var Router = Backbone.Router.extend({
        routes: {
            '': 'login',
            'home': 'home'
        },
        login: function () {
            console.log("shit");
        },
        home: function () {
            console.log("wut")
            var homeView = new HomeView();
            homeView.render();
        }
    });

    var router = new Router();

    var loginView = new LoginView();
    var homeView = new HomeView();

    router.on('route:login', function () {
        loginView.render();
    });

    router.on('route:home', function () {
        homeView.render();
    });

    Backbone.history.start();
});